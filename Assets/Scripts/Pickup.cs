﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour
{
    private Rigidbody rigidBody;

    // Use this for initialization
    void Start()
    {
        rigidBody = GetComponent<Rigidbody>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Hand"))
        {
            Grab(other);
        }
    }

    void Grab(Collider other)
    {
        rigidBody.isKinematic = true;
        rigidBody.detectCollisions = false;

        // Add as a child to the hand object
        transform.parent = other.transform;
    }

}
