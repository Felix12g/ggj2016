﻿using UnityEngine;
using System.Collections;


public class TrackerController : MonoBehaviour
{
    // Left = 0, Right = 1
    public short hand;
 
    private float sensitivity = 0.001f; // Sixense units are in mm

	private Transform hydraBaseReference = null;
	private bool hasInit = false;

	public Vector3 leftOffset = new Vector3(-0.05f, 0.05f, 0f);
	public Vector3 rightOffset = new Vector3(0.05f, 0.05f, 0f);

	public void Initialize(short handVal, Transform hydraBase)
	{
		hasInit = true;

		this.hand = handVal;

		hydraBaseReference = hydraBase;

		Vector3 offset = leftOffset;
		if (hand == 1)
			offset = rightOffset;
		transform.localPosition = (hydraBaseReference.position + offset);
	}

    void Update()
    {
		if (!hasInit)
			return;

        SixenseInput.Controller controller = SixenseInput.Controllers[hand];
        if (IsControllerActive(controller))
        {
			Vector3 offset = leftOffset;
			if(hand == 1)
				offset = rightOffset;

            transform.localPosition = (controller.Position * sensitivity) + (hydraBaseReference.position + offset);
            transform.localRotation = controller.Rotation * hydraBaseReference.rotation;
        }
    }

    bool IsControllerActive(SixenseInput.Controller controller)
    {
        return (controller != null && controller.Enabled && !controller.Docked);
    }
}
