﻿using UnityEngine;
using System;
using System.Collections;

[RequireComponent(typeof(Animator))]

public class IKController : MonoBehaviour {

	protected Animator animator;

	public bool ikActive = false;

	public bool leftTrackingEnabled = true;
	public Transform leftHandTracker = null;

	public bool rightTrackingEnabled = true;
    public Transform rightHandTracker = null;

	

    // Use this for initialization
    void Start () {
        animator = GetComponent<Animator>();
	}
	
    void OnAnimatorIK()
    {

        if (animator)
        {
            if (ikActive)
            {
 
                if (leftHandTracker != null && leftTrackingEnabled)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 1);
                    animator.SetIKPosition(AvatarIKGoal.LeftHand, leftHandTracker.position);
                    animator.SetIKRotation(AvatarIKGoal.LeftHand, leftHandTracker.rotation); 
                }

                if (rightHandTracker != null && rightTrackingEnabled)
                {
                    animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
                    animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
                    animator.SetIKPosition(AvatarIKGoal.RightHand, rightHandTracker.position);
                    animator.SetIKRotation(AvatarIKGoal.RightHand, rightHandTracker.rotation);
                }


            }
        } else
        {
            animator.SetIKPositionWeight(AvatarIKGoal.LeftHand, 0);
            animator.SetIKRotationWeight(AvatarIKGoal.LeftHand, 0);
        }
    }
}
