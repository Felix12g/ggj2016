﻿using UnityEngine;
using System.Collections;

public class TrackCamera : MonoBehaviour {
    public Camera myCam;
 
    // Update is called once per frame
	void Update () {
		if (myCam != null)
		{
			transform.position = new Vector3(myCam.transform.position.x, 0f, myCam.transform.position.z);
			transform.localEulerAngles = new Vector3(0f, myCam.transform.localEulerAngles.y, 0f);
		}
		else
		{
			myCam = Camera.main;
		}
    }
}
