﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChunkManager : MonoBehaviour
{

	#region Variables

	public GameManager gameManager = null;

	public List<DismemberedChunk> prettyBitPrefabs = new List<DismemberedChunk>();
	public List<BodyChunk> bodyPartPrefabs = new List<BodyChunk>();

	#endregion

	public void Init(GameManager manager)
	{
		gameManager = manager;
	}

	public DismemberedChunk GetDismemberedBit(ChunkType cType)
	{
		//TODO - Set this up to go through prefabs for a type
		return prettyBitPrefabs[0];
	}

	public DismemberedChunk GetRandomDismemberedBit()
	{
		//TODO - Set this up to grab a random prefab
		return prettyBitPrefabs[0];
	}

	public BodyChunk GetBodyPart(ChunkType cType)
	{
		//TODO - Set this up to go through prefabs for a type
		return bodyPartPrefabs[0];
	}

	public BodyChunk GetRandomBodyPart()
	{
		//TODO - Set this up to grab a random prefab
		return bodyPartPrefabs[0];
	}
}
