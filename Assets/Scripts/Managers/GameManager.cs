﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SM = UnityEngine.SceneManagement;

/// <summary>
/// This component should go in the base scene and handle gameplay initialization and inputs
/// </summary>
public class GameManager : MonoBehaviour
{
	#region Variables

	public Transform playerSpawnPoint = null;
	public GameObject playerPrefab = null;
	private MrChunkyController playerController = null;

	public GameObject cameraPrefab = null;
	public Transform mainCam = null;

	private bool hasInit = false;

	public ChunkSprayer2 chunkSprayer = null;

	public Transform hydraBaseStation = null;

	public OopsCounter oopsCounter = null;

	public int maxmimumOopsAllowed = 1000;

	private bool changingScene = false;

	public List<TrackerController> staticTrackers = new List<TrackerController>();

	public List<AudioClip> dismemberClips = new List<AudioClip>();
	public List<AudioClip> choppedClips = new List<AudioClip>();

	public float levelLoadDelay = 10f;
	public float endAnimShowoffDelay = 5f;

	public GUITexture screenFadeTexture = null;
	public float screenFadeTime = 1.5f;
	public bool screenFading = false;
	public Color fadeTargetColor = Color.red;

	public bool isTitleScreen = false;
	public AudioSource titleScreenAudioSource = null;

	#endregion

	#region Init

	void OnLevelWasLoaded()
	{
		if(!hasInit)
		{
			InitGame();
		}
	}

	void Start ()
	{
		if(!hasInit)
		{
			InitGame();
		}
	}

	void InitGame()
	{
		if(hasInit)
		{
			return;
		}
		hasInit = true;
		screenFading = true;

		if(screenFadeTexture != null)
		{
			FadeToClear();
		}

		if (!isTitleScreen)
		{
			//Generate the camera
			GameObject camObj = (GameObject)Instantiate(cameraPrefab, Vector3.zero, Quaternion.identity);
			mainCam = camObj.transform;
			Debug.Log("GameManager cam obj set");
			//Point the sprayer at the camera
			chunkSprayer.target = mainCam;

			foreach (TrackerController tr in staticTrackers)
			{
				tr.Initialize(tr.hand, hydraBaseStation);
			}
		}
		//If the player hasn't been generated yet, make one
		if(playerController == null)
		{
			if(playerPrefab == null)
			{
				Debug.LogError("Attach the bloody player prefab."); //Or soon to be
				return;
			}

			Vector3 spawnPosition = Vector3.zero;
			Quaternion spawnRotation = Quaternion.identity;
			if(playerSpawnPoint != null)
			{
				spawnPosition = playerSpawnPoint.position;
				spawnRotation = playerSpawnPoint.rotation;
			}
			GameObject playerObj = (GameObject)Instantiate(playerPrefab, spawnPosition, spawnRotation);
			if(playerObj != null)
			{
				playerController = playerObj.GetComponent<MrChunkyController>();
				playerController.InitCharacter(this);
			}

		}
		else
		{
			playerController.gameManager = this;
		}

		if(oopsCounter != null && oopsCounter.name != "OopsCounterPrefabMod")
		{
			oopsCounter.UpdateOopsCount("None");
			oopsCounter.UpdateOopsColor(Color.green);
		}
		else
		{
			Debug.LogWarning("Init::Oops counter not set on GameManager");
		}
	}



	#endregion

	public AudioClip GetRandomDismemberClip()
	{
		return dismemberClips[Random.Range(0, dismemberClips.Count - 1)];
	}

	public AudioClip GetRandomChoppedClip()
	{
		return choppedClips[Random.Range(0, choppedClips.Count - 1)];
	}

	public MrChunkyController GetPlayerController()
	{
		return playerController;
	}

	public void SprayTime(string objectChipped)
	{
        //This one gives
		chunkSprayer.StartSprayNow("Head_Skin");
	}

	public void UpdateOops()
	{
		if(oopsCounter == null)
		{
			Debug.LogError("GameManager needs to reference this scene's oops counter.");
			return;
		}

		int chunksEmitted = chunkSprayer.chunksEmitted + chunkSprayer.exactChunksReturned;

		bool fireEndGame = false;

		string newOops = chunksEmitted.ToString() + " / " + maxmimumOopsAllowed; 
		if(chunksEmitted >= maxmimumOopsAllowed)
		{
			fireEndGame = true;
			newOops = "Oops!";
		}

		oopsCounter.UpdateOopsCount(newOops);
		oopsCounter.UpdateOopsColor(Color.red);

		if(fireEndGame)
		{
			ChangeScene();
		}
	}

	void ChangeScene()
	{
		if(changingScene)
		{
			return;
		}
		changingScene = true;

		if (isTitleScreen)
		{
			titleScreenAudioSource.Play();
		}
		else
		{
			playerController.oopsAudioSource.Play();
		}

		StartCoroutine("DoSceneChangeTransition", levelLoadDelay);
	}

	IEnumerator DoSceneChangeTransition(float delay)
	{
		if (!isTitleScreen)
		{
			FadeToBlack();
		}

		yield return new WaitForSeconds(delay);

		//Delay in here to switch scenes
		//Fade out
		//Audio

		if (isTitleScreen)
		{
			StartCoroutine("RunAnimFollowupDelay", endAnimShowoffDelay);
		}
		else
		{
			LoadNextScene();
		}
	}

	IEnumerator RunAnimFollowupDelay(float delay)
	{
		yield return new WaitForSeconds(delay);

		FadeToBlack();

		LoadNextScene();
	}

	void FadeToClear()
	{
		if(screenFadeTexture == null)
		{
			return;
		}
		Debug.Log("Starting FadeToClear");

		screenFading = true;
		fadeTargetColor = Color.clear;
	}

	void FadeToBlack()
	{
		if (screenFadeTexture == null)
		{
			return;
		}
		Debug.Log("Starting FadeToBlack");

		screenFading = true;
		fadeTargetColor = Color.black;
	}

	void LoadNextScene()
	{
		if(isTitleScreen)
		{
			SceneTransition.ReloadLevelAsync();
		}
		else
		{
			SceneTransition.NextLevelAsync();
		}
	}

	// Update is called once per frame
	void Update () {
		if(screenFading && screenFadeTexture != null)
		{
			screenFadeTexture.pixelInset = new Rect(0f, 0f, Screen.width, Screen.height);
			screenFadeTexture.color = Color.Lerp(screenFadeTexture.color, fadeTargetColor, screenFadeTime * Time.deltaTime);
		}
	}
}
