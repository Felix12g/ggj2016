﻿using UnityEngine;
using System.Collections;
using System;

public class FloatingToolController : MonoBehaviour
{
    // Left = 0, Right = 1
    public short hand;
    public GameObject brush;
    public GameObject cleaver;
 
    // Use this for initialization
    void Start()
    {
        brush.SetActive(true);
        cleaver.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (SixenseInput.Controllers[hand].Enabled)
        {
            if (SixenseInput.Controllers[hand].GetButtonDown(SixenseButtons.BUMPER | SixenseButtons.ONE | SixenseButtons.TWO | SixenseButtons.THREE | SixenseButtons.FOUR))
            {
                swap();
            }
        }
    }

    private void swap()
    {
        brush.SetActive(!brush.activeSelf);
        cleaver.SetActive(!brush.activeSelf);
    }
}
