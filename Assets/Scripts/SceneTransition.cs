﻿using UnityEngine;
using System.Collections;
//using GPC = OVRGamepadController;
using SM = UnityEngine.SceneManagement;

public class SceneTransition : MonoBehaviour
{
    public KeyCode NextLevelKey = KeyCode.N;
	public KeyCode ReloadLevelKey = KeyCode.R;
    public KeyCode QuitKey = KeyCode.Escape;

    void Update()
    {
        if (Input.GetKeyDown(QuitKey))
            Application.Quit();

        if (Input.GetKeyDown(NextLevelKey))
            NextLevelAsync();

        if (Input.GetKeyDown(ReloadLevelKey))
            ReloadLevelAsync();

        if (SixenseInput.Controllers[1].Enabled)
        {
            if (SixenseInput.Controllers[1].GetButtonDown(SixenseButtons.START) && Time.timeSinceLevelLoad > 1.0f) //Sixense getButtonDown gets fooled on frame 1 after level change
            {
                NextLevelAsync();

            }
        }
    }

    void OnLevelWasLoaded(int level)
    {
        VRToggles.recenterVR();

    }

    public static void NextLevelAsync()
    {
        int curLevel = Application.loadedLevel;
        int nextLevel = (curLevel + 1) % Application.levelCount;
        Debug.Log("Loading nextLevel:" + nextLevel +
                  " curLevel:" + curLevel +
                  //" fadeTime:" + fadeTime +
                  "");

        SM.SceneManager.LoadSceneAsync(nextLevel);
    }

	public static void ReloadLevelAsync()
	{
		int curLevel = Application.loadedLevel;
		Debug.Log("Re-Loading Level:" + curLevel);

		SM.SceneManager.LoadSceneAsync(curLevel);
	}
}

    