﻿using UnityEngine;
using System.Collections;

public enum ChunkType 
{
	ERROR,
	Head,
	Neck,
	Torso,
	Hips,
	LeftLeg_Thigh1
}
