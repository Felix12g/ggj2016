﻿using UnityEngine;
using System.Collections;

public class ChunkSprayer2 : MonoBehaviour {
    //TODO: to calcuate force to hit DK2 at any distance:
    //Take XZ ground distance, calculate how long to reach target (no drag)
    //Calculate amount of drop over that time using gravity
    //Add force in Y (increasing overall force) so that gravity drop is offset

    //TODO: scale avatars based on avatar wrist dist in t-pose / tracked input distance in t-pose

    //TODO: aiming chunks
    public KeyCode testSprayKey = KeyCode.C;
    [Tooltip("attach tracked head to target, so we can aim chunks at it!")]
    public Transform target;
    public float chunkSpraySeconds = 1.0f;
    public float maxChunksPerFrame = 2;
    [Tooltip("Don't want evenly spaced chunk emit times. 0.9 means only emit 90% of each frame on average.")]
    public float chanceChunkPerFrame = 0.95f;
    public float newChunkSpeed = 2.0f;

//TODO: use these for more variety;
    [Tooltip("0.2 means chunks emit at newChunkSpeed + or - 20%")]
    public float newChunkSpeedVary = 0.1f;
    public float newChunkAimVary = 0.1f;
    public Vector3 newChunkRotationVary = new Vector3(1, 2, 3);

    [Tooltip("Chunks have RB, centered pivot, capsule collider, and script/class type DismemberedChunk")]
    public GameObject parentOfChunks;
    GameObject[] chunkList;
    string chunkNameMatch = null;

	public int chunksEmitted = 0;
	public int exactChunksReturned = 0;

    Vector3 emitPos;

    //StartSprayNow() can be called by external scripts, like one on a woodchipper OnCollide
    public void StartSprayNow(string nameMatch = null)
    {
        chunkNameMatch = nameMatch;
        isSpraying = true;
        sprayEndTime = Time.time + chunkSpraySeconds;
        Debug.Log("StartSpray until: " + sprayEndTime);
    }

    //You can move around the emit point separate from this.position
    public void SetEmitPosition(Vector3 newEmitPos)
    {
        emitPos = newEmitPos;
    }

    public void ResetEmitPosition()
    {
        emitPos = transform.position;
    }



    void Start()
    {
		chunksEmitted = 0;
		exactChunksReturned = 0;
        emitPos = transform.position;
        //Fill chunkList
        chunkList = new GameObject[parentOfChunks.transform.childCount];
        int i = 0;
        foreach (Transform chunk in parentOfChunks.transform)
        {
            chunkList[i] = chunk.gameObject;
            //Debug.Log("adding to chunkList: " + i + " " + chunk.name);
            i++;
        }
    }

    bool isSpraying = false;
    float sprayEndTime;

   void OnTriggerEnter(Collider other)
    {
        StartSprayNow();
    }


    void Update()
    {

        if (Input.GetKeyDown(testSprayKey))
        {
            StartSprayNow();
        }
        else if (isSpraying && Time.time > sprayEndTime)
        {
            //Debug.Log("DONE WITH CHUNKS!!!!!!!");
            isSpraying = false;
            chunkNameMatch = null;
        }

        if (isSpraying)
            spraySomeChunks();
    }

    public GameObject GetExactChunk(string inName)
    {
        for (int i = 0; i < chunkList.Length; i++)
        {
            if (chunkList[i].name.Equals(inName))
            {
				exactChunksReturned++;
                return chunkList[i];
            }
        }
//        Debug.Log("GetExactChunk " + inName + " not found, YOU GET A TORSO!!!!!!!!!1");
		exactChunksReturned++;
        return chunkList[29];
    }

    GameObject GetRandomChunkByName(string inName)
    {
        //Find all chunk names that match inName (have it as a substring)
        GameObject[] matchList = new GameObject[chunkList.Length];
        int matches = 0;
        for(int i=0; i<chunkList.Length; i++)
        {
            if(chunkList[i].name.IndexOf(inName) > 0)
            {
                matchList[matches] = chunkList[i];
                Debug.Log(inName + " Match " + matches + " " + matchList[matches]);
                matches++;
            }
        }

        if(matches == 0)
        {
//            Debug.Log("no matches, YOU GET A TORSO!!!!!!!!!1");
            return chunkList[29];
        }

        //Now, randomly pick from tha list of what matches
        GameObject chose = matchList[Random.Range(0, matchList.Length)];
        Debug.Log(" Chose " + chose);
        return chose;
    }


    GameObject GetRandomChunk()
    {
        int rand = Random.Range(0, chunkList.Length);
        GameObject chunk = chunkList[rand];
        //Debug.Log("RandomChunk: " + rand + " " + chunk.name);

        return chunk;
    }
    
    void spraySomeChunks()
    {
        for (int i = 0; i < maxChunksPerFrame; i++)
        {
            if (Random.value > chanceChunkPerFrame)
                continue;

            //Debug.Log("...chunk");
            Vector3 dirToTarget = target.position - transform.position;
            float distToTarget = dirToTarget.magnitude;

            dirToTarget.Normalize();

            GameObject chunkToSpawn = GetRandomChunk();
			//if (chunkNameMatch != null)
			//{
			//	chunkToSpawn = GetRandomChunkByName(chunkNameMatch);
			//}

            GameObject newChunk = (GameObject)Instantiate(chunkToSpawn, emitPos, Quaternion.identity);

			chunksEmitted++;

            Rigidbody rb = newChunk.GetComponent<Rigidbody>();
            rb.velocity = newChunkSpeed * distToTarget * dirToTarget;
            rb.angularVelocity = newChunkRotationVary;
        }
    }




    public static int EnableMeAndChildrenRB(Transform me)
    {
        int count = 0;
        Rigidbody rb = me.GetComponent<Rigidbody>();
        if (rb != null)
        {
            count++;
            rb.isKinematic = false;
            rb.useGravity = true;
        }
        foreach (Transform child in me.transform)
        {
            count += EnableMeAndChildrenRB(child);
        }

        return count;
    }



    //hack to put here, but static functions can be called form anywhere
    public static int DisableMeAndChildrenRB(Transform me)
    {
        int count = 0;
        Rigidbody rb = me.GetComponent<Rigidbody>();
        if (rb != null)
        {
            count++;
            //rb.detectCollisions = false;
            rb.isKinematic = true;
            rb.useGravity = false;
            //Debug.Log("Disabled RB: " + rb.name);
            //rb.GetComponent<Collider>().isTrigger = true;  //TODO: why didn't this work to fire events?
        }
        foreach (Transform child in me.transform)
        {
            count += DisableMeAndChildrenRB(child);
        }

        return count;
    }

}
