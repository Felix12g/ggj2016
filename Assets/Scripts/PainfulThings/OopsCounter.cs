﻿using UnityEngine;
using System.Collections;

public class OopsCounter : MonoBehaviour {

	public TextMesh txtMesh = null;

	public void UpdateOopsCount(string val)
	{
		txtMesh.text = val;
	}

	public void UpdateOopsColor(Color color)
	{
		txtMesh.color = color;
	}
}
