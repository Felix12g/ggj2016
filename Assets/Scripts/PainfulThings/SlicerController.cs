﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SlicerController : MonoBehaviour {

	public ChunkSprayer2 sprayer = null;
	public GameManager gameManager = null;

	public AudioSource audioSource = null;

	public float lastWoodChop = 0f;
	public float woodChopSoundDelay = 1f;
	public List<AudioClip> woodChopClips = new List<AudioClip>();

	public int oopsCount = 0;

	void OnTriggerEnter(Collider other)
	{

		if(other.tag == "Player" || other.tag == "Hand")
		{
			BodyChunk chunk = other.gameObject.GetComponent<BodyChunk>();
			if(chunk != null)
			{
				//Debug.Log("Cleaver hit " + other.gameObject.name);
				ChunkMeAndChildren(chunk);
			}
		}
		else if(other.tag == "Wood")
		{
			if(lastWoodChop + woodChopSoundDelay < Time.time)
			{
				PlayWoodChopSound();
			}
		}
	}

	void PlayWoodChopSound()
	{
		lastWoodChop = Time.time;
		AudioClip clip = woodChopClips[Random.Range(0, woodChopClips.Count - 1)];
		audioSource.PlayOneShot(clip);
	}

	void PlayChoppedSound()
	{
		if(gameManager != null)
		{
			AudioClip clip = gameManager.GetRandomChoppedClip();
			audioSource.PlayOneShot(clip);
		}
		else
		{
			Debug.LogWarning("Set reference to game manager if you want to play chopped sounds with cleaver");
		}
	}

    public int ChunkMeAndChildren(BodyChunk chunk)
    {

        chunk.rend.enabled = false;
        Transform bone = chunk.transform.parent;

		PlayChoppedSound();

        GameObject newChunk = sprayer.GetExactChunk(chunk.gameObject.name);
        //Instantiate(newChunk, chunk.transform.position, chunk.transform.rotation);
        Instantiate(newChunk, bone.position, bone.rotation);

		oopsCount++;

		gameManager.UpdateOops();

        int count = 0;

        Rigidbody rb = newChunk.GetComponent<Rigidbody>();
        if (rb != null)
        {
            count++;
            //rb.detectCollisions = false;
            rb.isKinematic = false;
            rb.useGravity = true;
            //Debug.Log("Disabled RB: " + rb.name);
            //rb.GetComponent<Collider>().isTrigger = true;  //TODO: why didn't this work to fire events?
        }


        //This is causing an endless loop, was trying to turn every child of type BodyChunk into an independent rigid body
        /*
        foreach (Transform child in bone.transform)
        {
            BodyChunk childChunk = child.gameObject.GetComponent<BodyChunk>();
            if(childChunk != null)
                count += ChunkMeAndChildren(childChunk);
        }
        */
        return count;
    }
}
