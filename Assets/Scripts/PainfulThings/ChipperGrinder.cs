﻿using UnityEngine;
using System.Collections;

public class ChipperGrinder : MonoBehaviour {

	public ChipperController chipper = null;
	public AudioSource audioSource = null;

	void OnTriggerEnter(Collider other)
	{
		if (chipper.GetChipperActive())
		{
			//Debug.Log("Grinder got a hit.");
			chipper.ChippinTime(other, audioSource);
		}
	}
}
