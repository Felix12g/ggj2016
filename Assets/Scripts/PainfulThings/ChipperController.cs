﻿using UnityEngine;
using System.Collections;

public class ChipperController : MonoBehaviour
{


	#region Variables

	public GameManager gameManager = null;

	public Rigidbody backGrinderRigid = null;
	public GameObject backGrinderObj = null;
	public Transform backGrinderXForm = null;
	public float backGrinderRotationSpeed = 30f;

	public Rigidbody frontGrinderRigid = null;
	public GameObject frontGrinderObj = null;
	public Transform frontGrinderXForm = null;
	public float frontGrinderRotationSpeed = 30f;

	public bool maximumPulp = false;

	public AudioSource audioSource = null;

	#endregion


	void Start ()
	{
		StartChipper();
	}
	
	void StartChipper()
	{
		maximumPulp = true;
		if(audioSource != null)
		{
			audioSource.Play();
		}
	}

	void StopChipper()
	{
		maximumPulp = false;
		if (audioSource != null)
		{
			audioSource.Stop();
		}
	}

	public bool GetChipperActive()
	{
		return maximumPulp;
	}

	void FixedUpdate()
	{
		if(maximumPulp)
		{
			backGrinderXForm.Rotate(Vector3.up, backGrinderRotationSpeed * Time.fixedDeltaTime);
			frontGrinderXForm.Rotate(Vector3.up, frontGrinderRotationSpeed * Time.fixedDeltaTime);
		}
	}

	public void ChippinTime(Collider other, AudioSource source)
	{
		if(!maximumPulp)
		{
			return;
		}
//		Debug.Log("Chipper got a hit: " + other.tag);

		if(other.tag == "Player" || other.tag == "Hand")
		{
			BodyChunk chunk = other.gameObject.GetComponent<BodyChunk>();
			if(chunk != null)
			{
//				Debug.Log("Chunk is not null on: " + other.name);
				bool attached = chunk.GetIsAttached();

				if(attached)
				{
					chunk.DismemberThisChunk(false, source);
					gameManager.SprayTime(chunk.gameObject.name);
				}
			}
		}
	}
}
