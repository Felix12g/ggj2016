﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BodyChunk : MonoBehaviour
{

#region Variables

	//Reference the base body's controller script
	public MrChunkyController bodyController = null;
	private bool isAttached = false;
	private bool hasInit = false;

	public CapsuleCollider col = null;

	public SkinnedMeshRenderer rend = null;
	public GameObject rendObj = null;

	public ChunkType chunkType = ChunkType.ERROR;

	//How big an oops losing this chunk is
	public int oopsAmount = 1;

	//Lower rank = more important, IE closer to head, 0 is head, 1 is neck, 2 is torso etc.
	public int chunkRank = 0;

	//Center is 0, Left side is 1, right side is 2, used to dismember pieces downrange from the point lost
	public int bodySide = 0;

	
	
	#endregion

	#region Init

	void Start()
	{
		if(col == null)
		{
			col = this.gameObject.GetComponent<CapsuleCollider>();

			if(col == null)
			{
				col = this.gameObject.AddComponent<CapsuleCollider>();
				Debug.LogWarning("No capsule collider found on body chunk: " + this.gameObject + "generating default one now.");
			}
		}
		if(rendObj == null)
		{
			if(rend == null)
			{
				Debug.LogError(this.gameObject.name + " does not have a render target set.");
			}
			else
			{
				rendObj = rend.gameObject;
			}
		}
	}

	public bool GetHasInit()
	{
		return hasInit;
	}

	public bool GetIsAttached()
	{
		return isAttached;
	}

	public void InitChunk(MrChunkyController control)
	{
		if(hasInit)
		{
			Debug.LogError("Calling init on body chunk that is already initialized.");
			return;
		}

		hasInit = true;

		bodyController = control;

		SetAttached(true);
	}

	private void SetAttached(bool val)
	{
		isAttached = val;
	}

	#endregion

	public void DismemberThisChunk(bool isCleaningUpPieces, AudioSource source)
	{
		if(!isAttached)
		{
			//Attempting to dismember a dismembered target
			return;
		}
		isAttached = false;

		if (col != null)
		{
			col.enabled = false;
		}

		if(rend != null)
		{
			rend.enabled = false;
		}
		if(rendObj != null)
		{
			rendObj.SetActive(true);
		}

		if (source != null)
		{
			bodyController.LostAChunk(this, isCleaningUpPieces);
		}


		if(source != null)
		{
			PlayDismemberAudio(source);
		}
	}

	private void PlayDismemberAudio(AudioSource source)
	{
		AudioClip clip = GetRandomDismemberClip();
		if(clip != null)
		{
			source.PlayOneShot(clip);
		}
	}

	public AudioClip GetRandomDismemberClip()
	{
		return bodyController.gameManager.GetRandomDismemberClip();
	}

	public void OnTriggerEnter(Collider other)
	{
		//Debug.Log("Body Chunk - Trigger entered.");
	}

	public void OnCollisionEnter(Collision col)
	{
		//Debug.Log("Body Chunk - Collider hit: " + col.gameObject.tag);
	}
}
