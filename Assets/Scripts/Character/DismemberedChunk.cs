﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DismemberedChunk : MonoBehaviour
{

	#region Variables

	public GameObject obj = null;
	public Transform xForm = null;
	public ChunkType chunkType = ChunkType.ERROR;

	private CapsuleCollider col = null;
	private Rigidbody rigid = null;
	public float defaultMass = 3f;
	public float defaultDrag = 0.1f;
	public float defaultAngDrag = 0.1f;

	#endregion

	void Start()
	{
		if (obj == null)
		{
			obj = this.gameObject;
		}
		if (xForm == null)
		{
			xForm = this.transform;
		}
		if (col == null)
		{
			col = obj.GetComponent<CapsuleCollider>();

			if (col == null)
			{
				col = obj.AddComponent<CapsuleCollider>();
			}
		}

	}

	public void InitChunk()
	{
		//Do stuff to a chunk when you generate it
	}
}
