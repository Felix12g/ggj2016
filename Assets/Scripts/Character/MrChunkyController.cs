﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MrChunkyController : MonoBehaviour
{

	#region Variables

	public List<BodyChunk> attachedChunks = new List<BodyChunk>();

	public List<BodyChunk> chunksLost = new List<BodyChunk>();

	private bool isActive = false;

	public Transform headMountParent = null;

	public GameObject handTrackerPrefab = null;

	public IKController ikController = null;

	public GameManager gameManager = null;

    public TrackCamera trackCam = null;

	public int totalOops = 0;

	public AudioSource oopsAudioSource = null;

	#endregion

	#region Init

	public void InitCharacter(GameManager manager)
	{
		if(isActive)
		{
			Debug.LogWarning("Calling InitCharacter on a controller that is already active. Returning.");
			return;
		}

		gameManager = manager;

		if (!manager.isTitleScreen)
		{
			isActive = true;
			gameManager.mainCam.position = headMountParent.position;
			gameManager.mainCam.rotation = headMountParent.rotation;
		}

		totalOops = 0;

		

		trackCam.myCam = Camera.main;

		if (ikController != null)
		{
			Vector3 startPos = Vector3.zero;
			Quaternion startRot = Quaternion.identity;
			if (gameManager.hydraBaseStation != null)
			{
				startPos = gameManager.hydraBaseStation.position;
				startRot = gameManager.hydraBaseStation.rotation;
			}

			if (ikController.leftTrackingEnabled)
			{
				GameObject leftHandTrackerObj = (GameObject)Instantiate(handTrackerPrefab, startPos, startRot);
				TrackerController leftController = leftHandTrackerObj.GetComponent<TrackerController>();
				leftController.Initialize(0, gameManager.hydraBaseStation);
				ikController.leftHandTracker = leftHandTrackerObj.transform;
			}

			if (ikController.rightTrackingEnabled)
			{
				GameObject rightHandTrackerObj = (GameObject)Instantiate(handTrackerPrefab, startPos, startRot);
				TrackerController rightController = rightHandTrackerObj.GetComponent<TrackerController>();
				rightController.Initialize(1, gameManager.hydraBaseStation);
				ikController.rightHandTracker = rightHandTrackerObj.transform;
			}

		}

		Component[] chunks = gameObject.GetComponentsInChildren<BodyChunk>();
		foreach(Component c in chunks)
		{
			BodyChunk chunk = (BodyChunk)c;
			if(chunk != null && !attachedChunks.Contains(chunk))
			{
				attachedChunks.Add(chunk);
			}
		}

		InitializeChunks();
	}

	void InitializeChunks()
	{
		foreach(BodyChunk chunk in attachedChunks)
		{
			if (!chunk.GetHasInit())
			{
				chunk.InitChunk(this);
			}
		}
	}

	#endregion

	public void LostAChunk(BodyChunk lostChunk, bool isCleaningUpPieces)
	{
		if (chunksLost.Contains(lostChunk))
		{
			//Already lost this chunk, shouldn't be called again
			return;
		}

		chunksLost.Add(lostChunk);

		if (!isCleaningUpPieces)
		{
			KnockOffLowerChunks(lostChunk);
		}

//		string newOops = GetOopsCount();

		gameManager.UpdateOops();
	}

	private string GetOopsCount()
	{
		int oopsValue = 0;
		foreach(BodyChunk c in chunksLost)
		{
			oopsValue += c.oopsAmount;
		}

		return oopsValue.ToString();
	}

	private void KnockOffLowerChunks(BodyChunk chunk)
	{
		foreach(BodyChunk c in attachedChunks)
		{
			if(c.bodySide == chunk.bodySide)
			{
				if(c.chunkRank > chunk.chunkRank)
				{
					c.DismemberThisChunk(true, null);
				}
			}
		}
	}
}
