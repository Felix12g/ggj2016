﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class VRToggles : MonoBehaviour
{

    public KeyCode resetTrackingKey = KeyCode.Space;
    public KeyCode toggleVRKey = KeyCode.V;
    public KeyCode toggleVRMirroringKey = KeyCode.M;

    //Must disable VR before first scene renders, or aspect is consistently destroyed for non-VR camera.
    //Usually? works in editor.  Ctrl-B never/rarely works.  Build, then run.
    void Start()
    {
        //toggleVRMirroring();
        //enableAllDisplays();
        //toggleVR();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(toggleVRMirroringKey))
            toggleVRMirroring();
        if (Input.GetKeyDown(toggleVRKey))
            toggleVR();
        if (Input.GetKeyDown(resetTrackingKey))
            recenterVR();

        if (SixenseInput.Controllers[0].Enabled) //The "back" button on left hydra for recenter
        {
            if (SixenseInput.Controllers[0].GetButtonDown(SixenseButtons.START))
            {
                recenterVR();
            }
        }
    }

    /*
    void addSceneAtOffset(string sceneName, Vector3 origin)
    {
        SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
        Scene s = SceneManager.GetSceneAt(SceneManager.sceneCount);
        //Scene.GetRootGameObjects() added in 5.3.1p1
    }
    */

    void toggleScene(string sceneName)
    {
        bool curLoaded = SceneManager.GetSceneByName(sceneName).isLoaded;
        Debug.Log("Toggling load for scene: " + sceneName + " to: " + !curLoaded);
        if (curLoaded)
            SceneManager.UnloadScene(sceneName);
        else
            SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Additive);
    }


    static void enableAllDisplays()
    {
        int dCount = Display.displays.Length;

        Debug.Log("Displays connected: " + dCount);
        for (int i = 1; i < dCount; i++)
        {
            Display.displays[i].Activate();
            Debug.Log("Activated display: " + dCount);
        }
    }

    public static void recenterVR()
    {
        Debug.Log("recenter VR");
        UnityEngine.VR.InputTracking.Recenter();
    }

    public static void toggleVR()
    {
        bool vrEnabled = UnityEngine.VR.VRSettings.enabled;
        vrEnabled = !vrEnabled;
        Debug.Log("Set vr enabled: " + vrEnabled);
        UnityEngine.VR.VRSettings.enabled = vrEnabled;
    }

    //Aspect ratio problem in 5.3.1f1.  
    void toggleVRMirroring()
    {
        bool mirroring = UnityEngine.VR.VRSettings.showDeviceView;
        mirroring = !mirroring;
        Debug.Log("Set vr mirroring: " + mirroring);
        UnityEngine.VR.VRSettings.showDeviceView = mirroring;
    }
}
